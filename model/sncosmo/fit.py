# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)


def fit_sncosmo_model(table_data, model_id, dict_params):
    """
    Fit an sncosmo model to a table of photometric data with code-supplied parameters.
    :param table_data: raw photometric data, formatted
    :type table_data: Table
    :param model_id: the name of the sncosmo model to use
    :type model_id: str
    :return: model_sncosmo_fit_metadata, model_sncosmo_fit: the fit model and its metadata
    """

    import sncosmo

    # fit sncosmo model to table_data
    # TODO: not assume the same dust law at both source and observer frames
    model_sncosmo_fit = sncosmo.Model(source=model_id, effects=dict_params["dust"]["effects"],
                                      effect_names=dict_params["dust"]["effect_names"],
                                      effect_frames=dict_params["dust"]["effect_frames"])

    # set loop_model_sncosmo_id parameters
    model_sncosmo_fit.set(z=dict_params["z"])
    model_sncosmo_fit.set(mwebv=dict_params["mwebv"])
    model_sncosmo_fit.set(mwr_v=dict_params["dust"]["mwr_v"])
    model_sncosmo_fit.set(hostr_v=dict_params["dust"]["hostr_v"])

    # fit sncosmo model
    # vparam_names: amplitude, hostebv, t0 (z is known)
    model_sncosmo_fit_metadata, model_sncosmo_fit = sncosmo.fit_lc(table_data,
                                                                   vparam_names={"amplitude", "hostebv", "t0"},
                                                                   guess_amplitude=True, model=model_sncosmo_fit,
                                                                   bounds={"hostebv": (dict_params["hostebv"]["lower"],
                                                                                       dict_params["hostebv"]["upper"]),
                                                                           't0': (dict_params["t0"]["lower"],
                                                                                  dict_params["t0"]["upper"])})

    return model_sncosmo_fit_metadata, model_sncosmo_fit


def fit_sncosmo_model_with_user_input(table_data, model_id):
    """
    Fit an sncosmo model to a table of photometric data with user-supplied parameters.
    :param table_data: raw photometric data, formatted
    :type table_data: Table
    :param model_id: the name of the sncosmo model to use
    :type model_id: str
    :return: model_sncosmo_fit_metadata, model_sncosmo_fit: the fit model and its metadata
    """

    import sncosmo

    # report the supernova name
    print("SN: " + table_data["event"][0])

    # fit sncosmo model to table_data
    # TODO: not assume the same dust law at both source and observer frames
    model_sncosmo_fit = sncosmo.Model(source=model_id, effects={sncosmo.CCM89Dust(), sncosmo.CCM89Dust()},
                                      effect_names={"host", "mw"},
                                      effect_frames={"rest", "obs"})

    # set loop_model_sncosmo_id parameters
    model_sncosmo_fit.set(z=input("z="))
    model_sncosmo_fit.set(mwebv=input("mwebv="))
    model_sncosmo_fit.set(mwr_v=input("mwr_v="))
    model_sncosmo_fit.set(hostr_v=input("hostr_v="))

    # fit sncosmo model
    # vparam_names: amplitude, hostebv, t0 (z is known)
    model_sncosmo_fit_metadata, model_sncosmo_fit = sncosmo.fit_lc(table_data,
                                                                   vparam_names={"amplitude", "hostebv", "t0"},
                                                                   guess_amplitude=True, model=model_sncosmo_fit,
                                                                   bounds={"hostebv": (input("lower hostebv bound="),
                                                                                       input("upper hostebv bound=")),
                                                                           't0': (input("lower t0 bound="),
                                                                                  input("upper t0 bound="))})

    return model_sncosmo_fit_metadata, model_sncosmo_fit


def get_best_fit_model(table_data, list_models):
    """
    Get the best fit sncosmo model for a table_data series.
    :param table_data: table_data that has been properly formatted
    :type table_data: Table
    :param list_models: list of models to use for fitting
    :type list_models: list
    :returns: name of the best-fitting sncosmo model (table_data series name, model metadata, and the model itself)
    """

    import numpy as np
    from tqdm import tqdm

    # external variable for tracking the best fit sncosmo model
    model_id_best_fit = "None"
    num_best_fit_chi_sq = np.inf

    # record the event name
    data_photometry_id = table_data["event"][0]

    # use tqdm to track the loop's progress
    status_pbar_loop_fitting = tqdm(desc="Fitting sncosmo models", total=len(list_models), unit=" models", leave=False)

    # loop through each sncosmo model
    for loop_model_sncosmo_id in list_models:
        # report which model is used for fitting
        status_pbar_loop_fitting.write("    New model: " + loop_model_sncosmo_id)

        # fit sncosmo model to table_data
        model_fit_metadata, model_fit = fit_sncosmo_model(table_data, loop_model_sncosmo_id)

        # report the χ² of this model's fit
        status_pbar_loop_fitting.write("      χ²: " + str(model_fit_metadata.chisq))

        # track best χ²
        if model_fit_metadata.chisq < num_best_fit_chi_sq:
            model_id_best_fit = loop_model_sncosmo_id
            num_best_fit_chi_sq = model_fit_metadata.chisq

        # update the model progress tracker
        status_pbar_loop_fitting.update(1)

    # close the model progress tracker
    status_pbar_loop_fitting.close()

    return model_id_best_fit, (data_photometry_id, model_fit_metadata, model_fit)


def get_all_best_fit_models(list_table_data, list_models, list_bands_fit):
    """
    Get the best fit sncosmo model for multiple table_data series.
    :param list_table_data: list of table_data series
    :type list_table_data: list
    :param list_models: list of models to use for fitting
    :type list_models: list
    :param list_bands_fit: list of photometric bands to use for model fitting
    :type list_bands_fit: list
    :returns: dictionary of table_data series names and best fit models, list of tuples of model metadata and model
    """

    from tqdm import tqdm

    # track best fit model for each event in a dictionary
    dict_ids_models_best = {}

    # save the best fit models in a dictionary
    dict_models_best = {}

    # set up main progress bar
    status_pbar_loop_data = tqdm(desc="Processing photometric table_data", total=len(list_table_data),
                                 unit=" series")

    # report into which photometric bands the sncosmo models will be fit
    status_pbar_loop_data.write("Fitting " + str(len(list_bands_fit)) + " bands: " + ", ".join(list_bands_fit))

    # loop through each photometric table_data series
    for loop_data_photometry in list_table_data:
        loop_data_phot_event_name = loop_data_photometry["event"][0]
        status_pbar_loop_data.write("New lightcurve: " + loop_data_phot_event_name)

        loop_id_model_best_fit, loop_tuple_model_best_fit = get_best_fit_model(loop_data_photometry,
                                                                               list_models)

        # identify the best fit loop_model_sncosmo_id for this event
        status_pbar_loop_data.write("  Best fit: " + loop_id_model_best_fit)

        # record best fit model
        dict_ids_models_best[loop_data_phot_event_name] = loop_id_model_best_fit
        dict_models_best[loop_data_phot_event_name] = loop_tuple_model_best_fit

        # update the event progress tracker
        status_pbar_loop_data.update(1)

    # close the event progress tracker
    status_pbar_loop_data.write("Best models found.")
    status_pbar_loop_data.close()

    return dict_ids_models_best, dict_models_best
