# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)


def get_vparams(model_results):
    """
    Extract vparams (varied parameters) and their values from an sncosmo model's table_data.
    :param model_results: results table_data of an sncosmo model
    :return: dictionary of vaparams
    """

    # only varied parameters (vparams) TODO: find way around range() in list comprehension
    dict_vparams = {model_results["param_names"][lc_index]: model_results["parameters"][lc_index] for lc_index in
                    range(len(model_results["param_names"])) if
                    model_results["param_names"][lc_index] in model_results["vparam_names"]}

    return dict_vparams


def get_parameters(model_results):
    """
    TODO: Complete get_parameters documentation
    :param model_results:
    :return:
    """

    # only provided parameters TODO: find way around range() in list comprehension
    dict_parameters = {model_results["param_names"][lc_index]: model_results["parameters"][lc_index] for lc_index in
                       range(len(model_results["param_names"])) if
                       model_results["param_names"][lc_index] not in model_results["vparam_names"]}

    return dict_parameters


def get_parameter_bounds(model_results, dict_vparams):
    """
    TODO: Complete get_parameter_bounds documentation
    :param dict_vparams:
    :param model_results:
    :return:
    """

    # turn the vparams and their error margins into bounds
    dict_bounds = {lc_key: (
        dict_vparams[lc_key] - model_results["errors"][lc_key], dict_vparams[lc_key] + model_results["errors"][lc_key])
        for lc_key in dict_vparams.keys()}

    return dict_bounds
