# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

# OAC to sncosmo
DICT_OAC_TO_SNCOSMO = {'u': "sdss::u",
                       'g': "sdss::g",
                       'r': "sdss::r",
                       'i': "sdss::i",
                       'z': "sdss::z"}
