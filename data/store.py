# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)


def model_per_event_to_csv(dict_ids_models_best, path_csv_models_best):
    """
    Save a CSV file with a model for each event.
    :param dict_ids_models_best: dictionary of events and sncosmo model IDs
    :type dict_ids_models_best: dict
    :param path_csv_models_best: path to save CSV to
    :type path_csv_models_best: Path
    """

    from zutils.file.write.csv import dict_to_csv

    # write best fit models to file
    dict_to_csv(dict_ids_models_best, path_csv_models_best, column_headers=["event", "model"])
