# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)


def cull_data(table_data, cull_bounds):
    """
    Remove all table_data after an amount of time from the first table_data point.
    :param table_data: astropy table of photometric table_data
    :param cull_bounds: Julian date bounds on the time of table_data
    :return: astropy table of table_data between tuple_culling_bounds
    """

    # limit to within cull_bounds days after the first table_data point
    table_data_modified = table_data[(table_data["time"] > cull_bounds[0]) & (table_data["time"] < cull_bounds[-1])]

    return table_data_modified


def add_zeropoint(table_data, zeropoint_magnitude=25.0, magnitude_system="ab"):
    """
    Add zeropoint table_data to table of photometric table_data.
    :param table_data: astropy table of photometric table_data
    :param zeropoint_magnitude: zeropoint magnitude
    :param magnitude_system: magnitude system for the zeropoint
    :return: astropy table with added zeropoint table_data
    """

    # add zeropoint_magnitude
    table_data_modified = table_data
    table_data_modified["zp"] = zeropoint_magnitude
    table_data_modified["zpsys"] = magnitude_system

    return table_data_modified


def add_magerr(table_data):
    """
    Add approximate magnitude error margin to table of photometric table_data.
    :param table_data: astropy table of photometric table_data
    :return: astropy table with added magnitude error margin
    """

    # add magnitude error margin
    # TODO: SCIENCE WARNING! Do not assume magerr of 0.75!
    table_data_modified = table_data
    table_data_modified["magerr"] = 0.75

    return table_data_modified


def add_flux(table_data):
    """
    Add flux table_data to table of photometric table_data.
    :param table_data: astropy table of photometric table_data
    :return: astropy table with added flux table_data
    """

    # add flux table_data
    table_data_modified = table_data
    # TODO: SCIENCE WARNING! Double check J. Pierel's math here.
    table_data_modified["flux"] = 10.0 ** (-0.4 * (table_data["magnitude"] - table_data["zp"]))
    table_data_modified["fluxerr"] = (0.92103 * table_data["e_magnitude"] * table_data["flux"])

    return table_data_modified


def explete_data(table_data, zeropoint_magnitude=25.0, magnitude_system="ab"):
    """
    Add both zeropoint and flux table_data to an astropy table of photometric table_data.
    :param table_data: astropy table of photometric table_data
    :param zeropoint_magnitude: zeropoint magnitude
    :param magnitude_system: magnitude system for the zeropoint
    :return: astropy table of photometric table_data with added zeropoint and flux table_data
    """

    # add zeropoint_magnitude table_data
    table_data_modified = add_zeropoint(table_data, zeropoint_magnitude=zeropoint_magnitude,
                                        magnitude_system=magnitude_system)

    # add magnitude error margin
    table_data_modified = add_magerr(table_data_modified)

    # add flux table_data
    table_data_modified = add_flux(table_data_modified)

    return table_data_modified


def format_data(table_data, list_bands_keep, cull_bounds):
    """
    Format photometric table_data for sncosmo model fitting.
    :param table_data: raw photometric table_data
    :param list_bands_keep: photometric bands to keep for sncosmo to fit
    :param cull_bounds: number of days after the first datum to keep
    :return: astropy table of raw, formatted photometric table_data
    """

    from snutils.ref.photometry.band_ciphers import DICT_OAC_TO_SNCOSMO
    from zutils.type.edit.astropy_tables import fill_masked_table, remove_rows_keep_value, replace_value_in_table

    # fill table_data Table() if masked to prevent dozens of type-mismatch headaches later
    table_data_filled = fill_masked_table(table_data, fill=None)

    # removed unwanted bands
    table_data_filtered = remove_rows_keep_value(table_data_filled, "band", list_bands_keep)

    # format table_data
    # limit to within 75 days after the first table_data point
    table_data_culled = cull_data(table_data_filtered, cull_bounds)

    # rename photometric bands
    table_data_renamed = replace_value_in_table(table_data_culled, "band", DICT_OAC_TO_SNCOSMO)

    # fill out additional table_data
    table_data_expleted = explete_data(table_data_renamed, zeropoint_magnitude=25.0, magnitude_system="ab")

    return table_data_expleted


def format_data_snsedextend(table_data, list_bands_keep, cull_bounds):
    """
    Format photometric table_data for snsedextend SED extension.
    :param table_data: raw photometric table_data
    :param list_bands_keep: photometric bands to keep for sncosmo to fit
    :param cull_bounds: number of days after the first datum to keep
    :return: astropy table of raw, formatted photometric table_data
    """

    from zutils.type.edit.astropy_tables import fill_masked_table, remove_rows_keep_value

    # fill table_data Table() if masked to prevent dozens of type-mismatch headaches later
    table_data_filled = fill_masked_table(table_data, fill=None)

    # removed unwanted bands
    table_data_filtered = remove_rows_keep_value(table_data_filled, "band", list_bands_keep)

    # format table_data
    # limit to within 75 days after the first table_data point
    table_data_culled = cull_data(table_data_filtered, cull_bounds)

    # fill out additional table_data
    table_data_expleted = explete_data(table_data_culled, zeropoint_magnitude=25.0, magnitude_system="ab")

    return table_data_expleted
