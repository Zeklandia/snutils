# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)


def format_all_photometry_data_series(arg_data_photometry_all, arg_bands_keep, arg_culling_time_length):
    """
    Format each entry in a list of table_data series.
    TODO: complete format_all_photometry_data_series documentation
    :param arg_data_photometry_all: list of unformatted table_data series
    :param arg_bands_keep:
    :param arg_culling_time_length:
    :return: list of formatted table_data series
    """

    from snutils.data.format import format_data

    # make a new list of formatted table_data series by iterating through the list of unformatted table_data series
    data_photometry_all_formatted = [
        format_data(lc_data_photometry, list_bands_keep=arg_bands_keep, cull_bounds=arg_culling_time_length)
        for lc_data_photometry in arg_data_photometry_all]

    return data_photometry_all_formatted


def format_all_photometry_data_series_snsedextend(list_data_photometry_all, list_bands_keep, tuple_culling_bounds):
    """
    Format each entry in a list of table_data series for use by snsedextend.
    TODO: complete format_all_photometry_data_series documentation
    :param list_data_photometry_all: list of unformatted table_data series
    :param list_bands_keep:
    :param tuple_culling_bounds:
    :return: list of formatted table_data series
    """

    from snutils.data.format import format_data_snsedextend

    # make a new list of formatted table_data series by iterating through the list of unformatted table_data series
    data_photometry_all_formatted = [format_data_snsedextend(lc_data_photometry, list_bands_keep=list_bands_keep,
                                                             cull_bounds=tuple_culling_bounds) for lc_data_photometry
                                     in list_data_photometry_all]

    return data_photometry_all_formatted
